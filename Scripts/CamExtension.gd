extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (NodePath) var cam_path
var parent_cam

onready var cam = get_node("View/Viewport/Camera")

# Called when the node enters the scene tree for the first time.
func _ready():
	parent_cam = get_node(cam_path)

func _process(delta):
	var origin = cam.transform.origin
	cam.transform = Transform(parent_cam.transform.basis, origin)

extends KinematicBody

class_name John

export (float) var gravity = 9.8
export (float) var fall_speed = 50.0
var current_fall_speed = 0.0

export (float) var move_speed = 6.0
export (float) var acceleration = 30.0
var current_speed = 0.0

export (float) var radius = 0.5

var cam: Camera;
var target: Vector3;
var moving: bool = false
var facing_down: bool = true

onready var sprite: AnimatedSprite3D = get_node("AnimatedSprite3D")

export (float) var collection_radius = 2.0
var desired_item = null

signal store_captcha(item)

func _ready():
	target = transform.origin
	cam = get_tree().root.get_camera()

func _physics_process(delta):	
	var space_state = get_world().direct_space_state
	var velocity = Vector3(0.0, 0.0, 0.0)
	var pos = transform.origin
	
	# Determine velocity X and Z based on motion towards target
	if moving:
		if current_speed == 0.0:
			var no_y_target = target;
			no_y_target.y = pos.y
			var direction = (no_y_target - pos).normalized()
			print_debug("Moving in direction: " + String(direction))
		
		# Accelerate and cap max speed
		current_speed += acceleration * delta
		current_speed = min(current_speed, move_speed)
		
		# We don't care about the Y position of our target location
		var no_y_target = target;
		no_y_target.y = pos.y
		
		# Determine direction towards target
		var direction = (no_y_target - pos).normalized()
		velocity.x  = direction.x * current_speed
		velocity.z = direction.z * current_speed
		
		# Set sprite angle based on horizontalness relative to the camera
		var right = cam.project_position(Vector2(1.0, 0.0)) - cam.project_position(Vector2(0.0, 0.0))
		right = right.normalized();
		if direction.dot(right) > 0.0:
			sprite.flip_h = false
		elif direction.dot(right) < 0.0:
			sprite.flip_h = true
			
		var up = cam.project_position(Vector2(0.0, 0.0)) - cam.project_position(Vector2(0.0, 1.0))
		up = up.normalized();
		if direction.dot(up) > 0.0:
			facing_down = false
		elif direction.dot(up) < 0.0:
			facing_down = true
		
		# Determine if we'll hit something on the way, stopping if so
		var dest = pos + velocity * delta
		var hit = space_state.intersect_ray(pos, dest + direction * radius, [self])
		
		if hit:
			moving = false
			current_speed = 0.0
			
		# Determine if we'll overshoot our destination, stopping if so
		var dist_to_target_sqr = (no_y_target - pos).length_squared()
		var dist_to_dest_sqr = (dest - pos).length_squared()
		if dist_to_dest_sqr > dist_to_target_sqr:
			moving = false
			current_speed = 0.0
		
	if moving:
		sprite.play("walk_down" if facing_down else "walk_up")
	else:
		sprite.play("idle_down" if facing_down else "idle_up")
		
	# Determine velocity Y based on gravity
	current_fall_speed += gravity * delta
	current_fall_speed = min(current_fall_speed, fall_speed)
	var gravity_dest = pos + Vector3.DOWN * current_fall_speed * delta
	var hit = space_state.intersect_ray(pos, gravity_dest + Vector3.DOWN * radius, [self])
	if hit:
		var dist_to_touch = (hit.get("position") - pos).length() - radius
		current_fall_speed = dist_to_touch / delta
	velocity.y = -current_fall_speed	
	
	move_and_slide(velocity)
	
	if desired_item != null:
		var dist_to_desires_sqr = (desired_item.transform.origin - transform.origin).length_squared()
		if dist_to_desires_sqr <= collection_radius * collection_radius:
			captcha(desired_item)
			desired_item = null
	
func move_to_target(newTarget: Vector3):
	var prev_dir = target - transform.origin
	target = newTarget
	var new_dir = target - transform.origin
	if prev_dir.dot(new_dir) < 0:
		current_speed = 0.0
	moving = true

func captcha(item):
	emit_signal("store_captcha", item)
	item.collect()

func try_to_collect(item):
	if item.has_method("collect"):
		desired_item = item
		move_to_target(item.transform.origin)
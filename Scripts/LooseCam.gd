extends Camera

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (NodePath) var target_path
export (float) var weight = 0.3
var initial_transform
var target

# Called when the node enters the scene tree for the first time.
func _ready():
	initial_transform = transform
	target = get_node(target_path)

func _process(_delta):
	var look_at_transform = transform.looking_at(target.transform.origin, Vector3.UP)
	var weighted_transform = Quat(initial_transform.basis).slerp(look_at_transform.basis, weight)
	transform = Transform(weighted_transform, initial_transform.origin)
	

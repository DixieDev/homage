extends TextureRect

class_name CaptchaCard

export (float) var transition_time = 0.3
var start_position: Vector2 = Vector2(0, 0)
var target_position: Vector2 = Vector2(0, 0)

var moving = false
var progress = 0.0

var fade_dir = "none"
var fade_progress = 0.0

func _ready():
	target_position = get_position()

func _process(delta):
	if moving:
		progress += delta
		if progress >= transition_time:
			progress = transition_time
			moving = false
		set_position(lerp(start_position, target_position, progress / transition_time))
	if fade_dir == "in":
		fade_progress += delta
		if fade_progress >= transition_time:
			fade_progress = transition_time
			fade_dir = "none"
		modulate = Color(modulate.r, modulate.g, modulate.b, fade_progress / transition_time)
	if fade_dir == "out":
		fade_progress -= delta
		if fade_progress <= 0:
			fade_progress = 0
			fade_dir = "none"
		modulate = Color(modulate.r, modulate.g, modulate.b, fade_progress / transition_time)
		
func move(dest: Vector2):
	start_position = get_position()
	target_position = dest
	moving = true
	progress = 0.0

func fade_in():
	fade_dir = "in"
	
func fade_out():
	fade_dir = "out"
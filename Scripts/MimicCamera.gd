extends Camera

export (NodePath) var cam_path
var cam

func _ready():
	cam = get_node(cam_path)

func _process(delta):
	rotation = cam.rotation


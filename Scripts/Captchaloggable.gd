extends Spatial

class_name Captchaloggable

onready var player: John = get_tree().root.find_node("John", true, false)

export (Texture) var captcha_texture: Texture

func collect():
	pass

func get_captcha_image():
	return captcha_texture

func _on_Area_input_event(camera, event, click_position, click_normal, shape_idx):
	if event.is_action_pressed("mouse_click"):
		player.try_to_collect(self)

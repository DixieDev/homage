extends Spatial

var player

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_tree().root.find_node("John", true, false)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_StaticBody_input_event(_camera, event, click_position, _click_normal, _shape_idx):
	if event.is_action_pressed("mouse_click"):
		player.move_to_target(click_position)
		print_debug("Clicked floor position: " + String(click_position))

extends MeshInstance

# Called when the node enters the scene tree for the first time.
func _ready():
	var viewport = $Viewport
	var tex = viewport.get_texture()
	mesh.surface_get_material(0).albedo_texture = $Viewport.get_texture()
	var flags = $Viewport.get_texture().flags 
	var flags_to_disable = tex.FLAG_ANISOTROPIC_FILTER | tex.FLAG_FILTER | tex.FLAG_MIPMAPS
	tex.flags = tex.flags & ~flags
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

extends Control

export (Vector2) var corner_padding: Vector2 = Vector2(5, 5)
export (Vector2) var captcha_offset: Vector2 = Vector2(25, 25)
export (int) var limit = 4
var contents: Array = []
var captcha_slots: Array = []

signal provide_item(item)
signal launch_item(item)

func add_item(item):
	# Push item onto stack
	contents.append(item)
	var num_cards = contents.size()
	
	# Create new card
	var new_card = CaptchaCard.new()
	new_card.set_position(Vector2(0, 0))
	new_card.texture = item.get_captcha_image()
	new_card.modulate = Color(1.0, 1.0, 1.0, 0.0)
	new_card.fade_in()	
	captcha_slots.append(new_card)
	add_child(new_card)
	
	# Start sliding transition for each current card
	for i in range(0, num_cards):
		captcha_slots[(num_cards-i)-1].move(corner_padding + captcha_offset * i)
		
	# Remove original item from the tree
	item.get_parent().remove_child(item)
	
	if num_cards > limit:
		eject_bottom()

func eject_bottom():
	var item = contents[0]
	captcha_slots[0].fade_out()
	contents.remove(0)
	captcha_slots.remove(0)
	emit_signal("launch_item", item)

func retrieve_top():
	var last = contents.size()-1
	var item = contents[last]
	captcha_slots[last].fade_out()
	captcha_slots[last].move(Vector2(0, 0))
	contents.remove(last)
	captcha_slots.remove(last)
	emit_signal("provide_item", item)
	
